var mongoose = require('mongoose');

exports.mongoose = function (args, done) {
    mongoose.connect('mongodb://localhost/' + args.database);

    var db = mongoose.connection;

    db.on('error', function (error) {
      console.error.bind(console, 'connection error:')
      done(error);
    });

    db.on('open', function () {
        done(null, {msg:'Connected to MongoDB'});
    });
};

var mongodb = require('mongodb');

var MongoClient = mongodb.MongoClient;
var state = {
    db: null
};

exports.connect = function (args, done) {
    if (state.db) {
        return done();
    } else {
        MongoClient.connect('mongodb://localhost:27017', function (error, client) {
            if (error) {
                return done(error);
            } else {
                state.db = client.db(args.database);
                done(null, state.db);
            }
        })
    }
};

exports.get = function () {
    return state.db;
};
