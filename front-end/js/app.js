var app = angular.module('App', ['ngRoute']);

app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    }
}]);

app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'views/home.html',
            controller: 'Home'
        })
        .when('/video/form', {
            templateUrl: 'views/form.html',
            controller: 'VideoForm'
        })
        .when('/video/list', {
            templateUrl: 'views/list.html',
            controller: 'VideoList'
        });
}]);

app.controller('VideoForm', ['$scope', '$http', function ($scope, $http) {
    $scope.video = {};
    $scope.create = function (video) {
        var data = new FormData();
        for (var key in video) {
            data.append(key, video[key]);
        }
        data.append('filename', $scope.file.name);
        data.append('video', $scope.file);
        $http.post('/api/videos', data, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(function (response) {
            console.log(response.data);
            $scope.video = {};
        });
    }
}]);

app.controller('VideoList', ['$scope', '$http', function ($scope, $http) {
    $scope.videos = {};
    $scope.retrieve = function () {
        $http.get('/api/videos')
            .then(function (response) {
               $scope.videos = response.data;
            });
    }
}]);

app.controller('Home', ['$scope', function ($scope) {
    $scope.title = 'Video Service';
}]);