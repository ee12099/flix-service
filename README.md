**Movies and Series stream service**

```
[TODO]
Introduce the app as a micro-service with a RESTful API,
try to explain this concepts.
```

---

## Flix Service ##

* [Node](https://nodejs.org/en/) - JavaScript runtime environment

* [MongoDB](https://www.mongodb.com/what-is-mongodb)  - Document Database

* [Express](https://expressjs.com/)  - Web Application Framework

* [Seneca](http://senecajs.org/)  - Micro-services Toolkit

* [Multer](https://github.com/expressjs/multer) -  Node.js middleware for handling multipart/form-data


---

### Project Structure ###

    .
    ├── config                   # Configuration files
    ├── models                   # Resource models
    ├── controllers              # Models controllers
    ├── routes                   # Web app resource routes
    ├── storage                  # File storage for resource files
    ├── server.js                # Server file
    ├── package.json             # node.js project package configuration file
    └── README.md

---

### Resource Model Structure ###

Each Resource is stored in a MongoDB JSON document and is defined by a model implemented with the Schema object provided by MongooseJS.

The main resources provided by this app are the Movie Resource and the Series Resource.

Each Movie contains information about the movie and a Video object plus an Array of Subtitle objects.

* Movie Resource
```
  Movie
    ├── video                           # Video Object
    ├── subtitles                       # Subtitle Array
```

Each Series contains information about the series and an Array of Season objects.
Thereby each Season object in the array contains information about the season and an Array of Episode object.

The Episode object follows the same logic as the Movie object. It contains information about the episode and a Video object plus an Array of Subtitles objects.

* Series Resource
```
  Series                                # Series Object
    ├── seasons                         # Season Array
          Season                        # Season Object
            ├── episodes                # Episode Array
              Episode                   # Episode Object
                ├── video               # Video Object
                ├── subtitles           # Subtitle Array
```

---

### Controller ###

The controller is the middle-ware between our routing endpoints and
our models.

Each controller as five main basic methods to operate in the model.

To create a new object of the specified model using
the provided object:

    addModel(args: {model: object})

To find the object based on id and read it:

    retrieveModel(args: {id: object_id})

To read all the objects of the specified model:

    listModel()

Find the object based on the id
 and update it with the provided object:

    updateModel(args: {id: object_id, model: object})

Delete the object with the specified id
from the database:

    deleteModel(args: {id: object_id})
---
### Routing ###

RESTful API mapping.

The routing of the resources follows the Resource Model Structure.

* Movie Routes

```
/movie:

  /{movie_id}:

    /subtitles:

      /{subtitle_id}:
```
For each Resource Model we create a main route.
In this case for the Movie model the main route '/movies'
has two attached methods.
One to act on HTTP GET Request (which sends
a HTTP Response with all the collection of Movie objects in the database) and
another one to act on HTTP POST Request (which creates a Movie object using the
  request body params and saves it in the database).
```
/movies:
  get:
    responses:
      200:
        body:
          application/json:
            type: Movie
  post:
    body:
      application/json:
        type: Movie
  /{movie_id}
    ...
```
To perform operations at the element level of a collection each element of
the collection is mapped to it's resource routes using it's id.
We can retrieve a specific Movie object passing it's id after the main route:
```
example: http://localhost:8080/api/movies/5a8497ecb3354b2486247875
```
To read the Movie send a HTTP GET Request, to update the Movie send a HTTP POST Request with an attached Movie object in the body, and to delete the Movie send a HTTP DELETE Request.
```
/movies
  ...       
  /{movie_id}:
    get:
      responses:
        200:
          body:
            application/json:
              type: Movie
    put:
      body:
        application/json:
          type: Movie
    delete:
      responses:
        204:
```

* Series Routes

```
/series:

  /{series_id}:

    /seasons:

      /{season_id}:

        /episodes:

          /{episode_id}:

            /subtitles:

              /{subtitle_id}:
```

* Video Routes

The Video Model has two additional routes at the element level.
The '/stream' route is provided to stream the respective video file and
the '/download' route to download it.

```
/video:

  /{video_id}:

    /stream:

    /download:
```

This service api is documented using RAML 1.0 Specification. Complete RAML file: [api.raml](https://bitbucket.org/ee12099/back-end/src/aedbde65e88e63b9fd105dc69d2108aaebba9f61/api.raml?at=master&fileviewer=file-view-default)

---

## Development ##

### Create a Model ###

Let's create a resource model for the Episode object.

To create our model we will use [MongooseJS](http://mongoosejs.com/). Mongoose is
an Object Document Mapper(ODM) which allows us to define objects with a model-typed schema that are mapped to MongoDB documents.

First import the MongooseJS library.
```javascript
var mongoose = require('mongoose');

```
Declare Schema variable as MongooseJS Schema class object.
```javascript
var Schema = mongoose.Schema;

```
Declare the resource model schema and populate it with attributes.
```javascript
var episodeSchema = new Schema({
    season: {type: Schema.Types.ObjectId, ref:'Season'},
    index: Number,
    title: String,
    description: String,
    cover_url: String,
    video: {},
    subtitles: [],
    timestamp: {
        created_at: Date,
        updated_at: Date
    }
});

```
The season attribute value is a reference to the parent Season object.
The mapping is done by the Season object id attribute.
```javascript
var episodeSchema = new Schema({
  season: {type: Schema.Types.ObjectId, ref:'Season'},
  ...
});

```
To declare an attribute write the key name followed by the Schema Type object for that attribute value.
```javascript
var episodeSchema = new Schema({
    ...
    index: Number,
    title: String,
    ...
});

```
You can declare an attribute as an Array. In this case the subtitles Array represents the collection of Subtitle objects that are children of this Episode object.
```javascript
var episodeSchema = new Schema({
    ...
    subtitles: [],
    ...
});

```
This method is called before the object is saved in the MongoDB, this means that it is called when the object is saved for the first time and every time and it is updated.
In this method we update the updated_at timestamp of this object and if the object is being saved for the first time declare the created_at timestamp.
```javascript
episodeSchema.pre('save', function (next) {
  var currentDate = new Date();
  this.timestamp.updated_at = currentDate;
  if (!this.timestamp.created_at) {
      this.timestamp.created_at = currentDate;
  }
});

```
Create the MongooseJS Model object with our defined episodeSchema schema.
This model can be used to query the MongoDB.
```javascript
var Episode = mongoose.model('Episode', episodeSchema);
```
Assign the Episode and the episodeSchema objects to the exports object.
```javascript
module.exports.Episode = Episode;
module.exports.episodeSchema = episodeSchema;
```
Complete Model file: [models/episode.js](https://bitbucket.org/ee12099/back-end/src/43c56d4bf8d7ec6ade9e4c74ba68bdfae5f9b155/models/episode.js?at=master&fileviewer=file-view-default)

### Create a Controller ###

Now that we have created an Episode MongooseJS Model lets create a controller to perform CRUD(Create Read Update Delete) operations.

First import the Episode and the Video MongooseJS Model.
```javascript
var Episode = require('../models/episode').Episode;
var Video = require('../models/video').Video;
```
Let's declare a method to create a new Episode object and save it in the MongoDB using MongooseJS Episode Model created previously.

Declare the addEpisode function and assign it to the exports object.
The methods declared in this controller will be injected by seneca.js in the routes objects. The parameter args is provided when seneca calls this method. The parameter done is a callback function(error, return).
```javascript
exports.addEpisode = function (args, done) {

};
```
To link the Episode object with its child Video object a video_id is provided.
The MongooseJS Video Model querys the MongoDB using this video_id and retrieves the respective video object.
If an error occurs pass it back to seneca through the callback function.
```javascript
exports.addEpisode = function (args, done) {
    Video.findById(args.video_id, function (error, video) {
        if (error) {
            done(error);
        } else {
            ...
        }
    });
  }
```
If the video was found create a new Episode object with the specified attributes and the values provided by seneca args parameter.
The retrieved video will be added to the Episode object in the video attribute value.
```javascript
var episode = new Episode({
    season: args.season,
    index: args.episode.index,
    title: args.episode.title,
    description: args.episode.description,
    cover_url: args.episode.cover_url,
    video: video
});
```
After creating the Episode object save it to the database using
MongooseJS Model save method.
If no error is called pass the episode variable in the callback function.
```javascript
episode.save(function (error) {
    if (error) {
        done(error);
    } else {
        done(null, episode);
    }
});
```
Complete addEpisode method declaration.
```javascript
exports.addEpisode = function (args, done) {
    Video.findById(args.video_id, function (error, video) {
        if (error) {
            done(error);
        } else {
            var episode = new Episode({
                season: args.season,
                index: args.episode.index,
                title: args.episode.title,
                description: args.episode.description,
                cover_url: args.episode.cover_url,
                video: video
            });
            episode.save(function (error) {
                if (error) {
                    done(error);
                } else {
                    done(null, episode);
                }
            });

        }
    });
  }
```

Let's create a method to add a Subtitle object to an Episode object.

```
[TODO]
Describe addSubtitleToEpisode method.
```


Complete addSubtitleToEpisode method.
```javascript
exports.addSubtitleToEpisode = function (args, done) {
  Episode.findById(args.episode, function (error, episode) {
      if (error) {
          done(error);
      } else {
          var index = episode.subtitles.map(function (element) {
              return element.subtitle;
          }).indexOf(args.subtitle);
          episode.subtitles[index] = args.subtitle;
          episode.save(function (error) {
              if (error) {
                  done(error);
              } else {
                  done(null, episode);
              }
          });
      }
  });
};
```
Complete Controller file: [controllers/episode.js](https://bitbucket.org/ee12099/back-end/src/924d00af31b79151c2221336c702439da0b6f3fd/controllers/episode.js?at=master&fileviewer=file-view-default)

```javascript
```
```javascript
```
```javascript
```
```javascript
```
```javascript
```
```javascript
```
```javascript
```
```javascript
```
```javascript
```
```javascript
```
```javascript
```
```javascript
```
```javascript
```
