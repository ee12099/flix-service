var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var seasonSchema = require('./season').seasonSchema;

var seriesSchema = new Schema({
    title: String,
    description: String,
    year: Number,
    genre: [String],
    director: [String],
    writer: [String],
    cast: [String],
    cover_url: String,
    seasons: [seasonSchema]
});

var Series = mongoose.model('Series', seriesSchema);

module.exports.Series = Series;
module.exports.seriesSchema = seriesSchema;

