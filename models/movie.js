var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var videoSchema = require('./video').videoSchema;

var movieSchema = new Schema({
    title: String,
    description: String,
    year: Number,
    genre: [String],
    director: [String],
    writer: [String],
    cast: [String],
    cover_url: String,
    video: {},
    subtitles: [],
    timestamp: {
        created_at: Date,
        updated_at: Date
    }
});

movieSchema.pre('save', function (next) {
    var currentDate = new Date();
    this.timestamp.updated_at = currentDate;
    if (!this.timestamp.created_at) {
        this.timestamp.created_at = currentDate;
    }
    next();
});

var Movie = mongoose.model('Movie', movieSchema);

exports.Movie = Movie;
exports.movieSchema = movieSchema;
