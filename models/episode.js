var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var episodeSchema = new Schema({
    season: {type: Schema.Types.ObjectId, ref:'Season'},
    index: Number,
    title: String,
    description: String,
    cover_url: String,
    video: {},
    subtitles: [],
    timestamp: {
        created_at: Date,
        updated_at: Date
    }
});

episodeSchema.pre('save', function (next) {
  var currentDate = new Date();
  this.timestamp.updated_at = currentDate;
  if (!this.timestamp.created_at) {
      this.timestamp.created_at = currentDate;
  }
});

var Episode = mongoose.model('Episode', episodeSchema);

module.exports.Episode = Episode;
module.exports.episodeSchema = episodeSchema;
