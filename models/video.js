var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var videoSchema = new Schema({
    urls: {
        stream_url: String,
        download_url: String
    },
    storage: {
        filename: String,
        path: String
    },
    timestamp: {
        created_at: Date,
        updated_at: Date
    }
});

videoSchema.pre('save', function (next) {
    var currentDate = new Date();
    this.timestamp.updated_at = currentDate;
    if (!this.timestamp.created_at) {
        this.timestamp.created_at = currentDate;
    }

    this.storage.path = '/videos/' + this.storage.filename.replace(/\.[^/.]+$/, "") + '/' + this.storage.filename;
    this.urls.stream_url = 'http://localhost:5000/api/videos/' + this._id + '/stream';
    this.urls.download_url = 'http://localhost:5000/api/videos/' + this._id + '/download';
    next();
});

var Video = mongoose.model('Video', videoSchema);

exports.Video = Video;
exports.videoSchema = videoSchema;