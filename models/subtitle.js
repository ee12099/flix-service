var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var subtitleSchema = new Schema({
  title: String,
  language: String,
  urls: {
      stream_url: String,
    download_url: String
  },
  storage: {
    filename: String,
    path: String
  },
  timestamp: {
    created_at: Date,
    updated_at: Date
  }
});

subtitleSchema.pre('save', function (next) {
  var currentDate = new Date();
  this.timestamp.updated_at = currentDate;
  if (!this.timestamp.created_at) {
      this.timestamp.created_at = currentDate;
  }

  this.storage.path = '/subtitles/' + this.storage.filename.replace(/\.[^/.]+$/, "") + '/' + this.storage.filename;
    this.urls.stream_url = 'http://localhost:5000/api/subtitles/' + this._id + '/stream';
    this.urls.download_url = 'http://localhost:5000/api/subtitles/' + this._id + '/download';
  next();
});

var Subtitle = mongoose.model('Subtitle', subtitleSchema);

exports.Subtitle = Subtitle;
exports.subtitleSchema = subtitleSchema;
