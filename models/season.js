var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var episodeSchema = require('./episode').episodeSchema;

var seasonSchema = new Schema({
    series: {type: Schema.Types.ObjectId, ref:'Series'},
    season: Number,
    title: String,
    description: String,
    year: Number,
    cover_url: String,
    episodes: [episodeSchema]
});

var Season = mongoose.model('Season', seasonSchema);

module.exports.Season = Season;
module.exports.seasonSchema = seasonSchema;