var express = require('express');
var router = express.Router({mergeParams: true});
var episode = require('./episode');

var seneca = require('seneca')();
var seasons = require('../controllers/season');
var series = require('../controllers/series');

seneca.add({role:'season', cmd:'add'}, seasons.addSeason);
seneca.add({role:'season', cmd:'list'}, seasons.listSeasons);
seneca.add({role:'season', cmd:'retrieve'}, seasons.retrieveSeason);
seneca.add({role:'season', cmd:'update'}, seasons.updateSeason);
seneca.add({role:'season', cmd:'delete'}, seasons.deleteSeason);
seneca.add({role:'series', cmd:'addSeason'}, series.addSeasonToSeries);

router.route('/')
    .post(function (request, response) {
        seneca.act({
            role:'season',
            cmd:'add',
            season: request.body,
            series: request.params.id
        }, function (error, season) {
            if (error) {
                response.send(error);
            } else {
                seneca.act({
                  role:'season',
                  cmd:'addSeason',
                  season: season,
                  series: request.params.id
                }, function (error, series) {
                  if (error) {
                    response.send(error);
                  } else {
                    response.send(season);
                  }
                })
            }
        })
    })
    .get(function (request, response) {
        seneca.act({
            role:'season',
            cmd:'list',
            series: request.params.id
        }, function (error, seasons) {
            if (error) {
                response.send(error);
            } else {
                response.send(seasons);
            }
        })
    });

router.route('/:season_id')
    .get(function (request, response) {
        seneca.act({
            role:'season',
            cmd:'retrieve',
            series: request.params.id,
            id: request.params['season_id']
        }, function (error, seasons) {
            if (error) {
                response.send(error);
            } else {
                response.send(seasons);
            }
        })
    })
    .put(function (request, response) {
        seneca.act({
            role:'season',
            cmd:'update',
            series: request.params.id,
            id: request.params['season_id'],
            season: request.body
        }, function (error, seasons) {
            if (error) {
                response.send(error);
            } else {
                response.send(seasons);
            }
        })
    })
    .delete(function (request, response) {
        seneca.act({
            role:'season',
            cmd:'delete',
            series: request.params.id,
            id: request.params['season_id'],
            attached: request.query.attached
        }, function (error, result) {
            if (error) {
                response.send(error);
            } else {
                response.send(result);
            }
        })
    });

router.use('/:season_id/episode', episode);

module.exports = router;
