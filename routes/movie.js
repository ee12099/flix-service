var express = require('express');
var router = express.Router();

var subtitle_router = require('./subtitle');

var seneca = require('seneca')();
var movies = require('../controllers/movie');


seneca.add({role:'movie', cmd:'add'}, movies.addMovie);
seneca.add({role:'movie', cmd:'list'}, movies.listMovies);
seneca.add({role:'movie', cmd:'retrieve'}, movies.retrieveMovie);
seneca.add({role:'movie', cmd:'update'}, movies.updateMovie);
seneca.add({role:'movie', cmd:'delete'}, movies.deleteMovie);

router.route('/movies')
    .post(function (request, response) {
        seneca.act(
            {
                role:'movie',
                cmd:'add',
                movie: request.body,
                video_id: request.body.video_id
            }, function (error, movie) {
                if (error) {
                    response.send(error);
                } else {
                    response.send(movie);
                }
        })
    })
    .get(function (request, response) {
        seneca.act({
            role: 'movie',
            cmd: 'list',
            query: request.query
        }, function (error, movies) {
            if (error) {
                response.send(error);
            } else {
                response.send(movies);
            }
        })
    });

router.route('/movies/:id')
    .get(function (request, response) {
        seneca.act({role:'movie', cmd:'retrieve', id: request.params.id}, function (error, movie) {
            if (error) {
                response.send(error);
            } else {
                response.send(movie);
            }
        });
    })
    .put(function (request, response) {
        seneca.act({role: 'movie', cmd:'update', id: request.params.id, movie: request.body}, function (error, movie) {
            if (error) {
                console.log(error);
            } else {
                response.send(movie);
            }
        });
    })
    .delete(function (request, response) {
        seneca.act({role:'movie', cmd:'delete', id: request.params.id}, function (error, result) {
            if (error) {
                console.log(error);
            } else {
                response.send(result);
            }
        });
    });

router.get('/movies/:id/download', function (request, response) {
    seneca.act({role:'movie', cmd:'retrieve', id: request.params.id}, function (error, movie) {
        console.log(movie);
        if (error) {
            response.send(error);
        } else {
            response.download(path.join(__dirname + '/../storage' + movie.video.storage.path));
        }
    });
});

router.get('/movies/:id/stream', function (request, response) {
    seneca.act({role:'movie', cmd:'retrieve', id: request.params.id}, function (error, movie) {
        if (error) {
            response.send(error);
        } else {
            response.set('Content-Type', 'video/mp4');
            var rstream = fs.createReadStream(path.join(__dirname + '/../storage' + movie.video.storage.path));
            rstream.pipe(response);
        }
    });
});

router.use('/movies/:id', subtitle_router);

module.exports = router;
