var express = require('express');
var router = express.Router({mergeParams: true});

var subtitle_router = require('./subtitle');

var seneca = require('seneca')();
var episodes = require('../controllers/episode');
var seasons = require('../controllers/season');
var series = require('../controllers/episode');


seneca.add({role:'episode', cmd:'add'}, episodes.addEpisode);
seneca.add({role:'episode', cmd:'list'}, episodes.listEpisodes);
seneca.add({role:'episode', cmd:'retrieve'}, episodes.retrieveEpisode);
seneca.add({role:'episode', cmd:'update'}, episodes.updateEpisode);
seneca.add({role:'episode', cmd:'delete'}, episodes.deleteEpisode);
seneca.add({role:'season', cmd:'addEpisode'}, seasons.addEpisodeToSeason);
seneca.add({role:'series', cmd:'addSeason'}, series.addSeasonToSeries);

router.route('/')
    .post(function (request, response) {
        seneca.act({
            role: 'episode',
            cmd: 'add',
            video_id: request.body.video_id,
            season: request.params['season_id'],
            episode: request.body
        }, function (error, episode) {
            if (error) {
                response.send(error);
            } else {
                seneca.act({
                  role:'season',
                  cmd:'addEpisode',
                  season: request.params['season_id'],
                  episode: episode
                }, function (error, season) {
                  if (error) {
                    response.send(error);
                  } else {
                    seneca.act({
                      role:'series',
                      cmd:'addSeason',
                      series: request.params.id,
                      season: season
                    }, function (error, series) {
                      if (error) {
                        response.send(error);
                      } else {
                        response.send(episode);
                      }
                    })
                  }
                })
            }
        })
    })
    .get(function (request, response) {
        seneca.act({
            role: 'episode',
            cmd: 'list',
            season: request.params['season_id']
        }, function (error, episode) {
            if (error) {
                response.send(error);
            } else {
                response.set({
                    'Access-Control-Allow-Origin': '*',
                    'Acess-Control-Allow-Methods': 'GET',
                    'Access-Control-Allow-Headers': 'Content-Type'
                });
                response.send(episode);
            }
        })
    });

router.route('/:episode_id')
    .get(function (request, response) {
        seneca.act({
            role: 'episode',
            cmd: 'retrieve',
            season: request.params['season_id'],
            id: request.params['episode_id']
        }, function (error, episode) {
            if (error) {
                response.send(error);
            } else {
                response.send(episode);
            }
        })
    })
    .put(function (request, response) {
        seneca.act({
            role: 'episode',
            cmd: 'update',
            id: request.params['episode_id'],
            episode: request.body
        }, function (error, episode) {
            if (error) {
                response.send(error);
            } else {
                response.send(episode);
            }
        })
    })
    .delete(function (request, response) {

    });

router.use('/:episode_id/', subtitle_router);

module.exports = router;
