var express = require('express');
var router = express.Router();
var multer = require('multer');
var seneca = require('seneca')();
var videos = require('../controllers/video');
var path = require('path');
var fs = require('fs');


seneca.add({role:'video', cmd:'add'}, videos.addVideo);
seneca.add({role:'video', cmd:'list'}, videos.listVideos);
seneca.add({role:'video', cmd:'retrieve'}, videos.retrieveVideo);
seneca.add({role:'video', cmd:'update'}, videos.updateVideo);
seneca.add({role:'video', cmd:'delete'}, videos.deleteVideo);


/*
    video storage only works if provided filename
    is the same as the uploaded file filename
 */
var videoStorage = multer.diskStorage({
    destination: function (request, file, callback) {
        var dir = file.originalname.replace(/\.[^/.]+$/, "");
        fs.mkdirSync(path.join(__dirname + '/../storage/videos/' + dir));
        callback(null, './storage/videos/' + dir);
    },
    filename: function (request, file, callback) {
        if (request.body.filename) {
            callback(null, request.body.filename);
        } else {
            callback(null, file.originalname);
        }
    }
});

var uploadVideo = multer({
    storage: videoStorage,
    fileFilter: function (request, file, callback) {
        if (file.mimetype.split('/')[0] !== 'video') {
            callback(new Error('Not a video file!'), false);
        }
        callback(null, true);
    }
});

router.route('/videos')
    .post(uploadVideo.single('video'), function (request, response) {
        seneca.act({role:'video', cmd: 'add', video: request.body}, function (error, video) {
           if  (error) {
               response.send(error);
           } else {
               response.send(video);
           }
        });
    })
    .get(function (request, response) {
        seneca.act({role: 'video', cmd: 'list', query: request.query}, function (error, videos) {
            if (error) {
                response.send(error);
            } else {
                response.set({
                    'Access-Control-Allow-Origin': '*',
                    'Acess-Control-Allow-Methods': 'GET',
                    'Access-Control-Allow-Headers': 'Content-Type'
                });
                response.send(videos);
            }
        })
    });

router.route('/videos/:id')
    .get(function (request, response) {
        seneca.act({role:'video', cmd:'retrieve', id: request.params.id}, function (error, video) {
            if (error) {
                response.send(error);
            } else {
                response.send(video);
            }
        });
    })
    .put(function (request, response) {
        seneca.act({role: 'video', cmd:'update', id: request.params.id, video: request.body}, function (error, video) {
           if (error) {
               response.send(error);
           } else {
               response.send(video);
           }
        });
    })
    .delete(function (request, response) {
        seneca.act({role:'video', cmd:'delete', id: request.params.id}, function (error, result) {
           if (error) {
               response.send(error);
           } else {
               response.send(result);
           }
        });
    });

router.get('/videos/:id/download', function (request, response) {
    seneca.act({role:'video', cmd:'retrieve', id: request.params.id}, function (error, video) {
        console.log(video);
        if (error) {
            response.send(error);
        } else {
            response.download(path.join(__dirname + '/../storage' + video.storage.path));
        }
    });
});

router.get('/videos/:id/stream', function (request, response) {
    seneca.act({role:'video', cmd:'retrieve', id: request.params.id}, function (error, video) {
        if (error) {
            response.send(error);
        } else {
            response.set('Content-Type', 'video/mp4');
            var rstream = fs.createReadStream(path.join(__dirname + '/../storage' + video.storage.path));
            rstream.pipe(response);
        }
    });
});


module.exports = router;