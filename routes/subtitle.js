var express = require('express');
var router = express.Router();
var multer = require('multer');
var seneca = require('seneca')();
var subtitles = require('../controllers/subtitle');
var episodes = require('../controllers/episode');
var path = require('path');
var fs = require('fs');


seneca.add({role:'subtitle', cmd:'add'}, subtitles.addSubtitle);
seneca.add({role:'subtitle', cmd:'list'}, subtitles.listSubtitles);
seneca.add({role:'subtitle', cmd:'retrieve'}, subtitles.retrieveSubtitle);
seneca.add({role:'subtitle', cmd:'update'}, subtitles.updateSubtitle);
seneca.add({role:'subtitle', cmd:'delete'}, subtitles.deleteSubtitle);


/*
    subtitle storage only works if provided filename
    is the same as the uploaded file filename
 */
var subtitleStorage = multer.diskStorage({
    destination: function (request, file, callback) {
        var dir = file.originalname.replace(/\.[^/.]+$/, "");
        fs.mkdirSync(path.join(__dirname + '/../storage/subtitles/' + dir));
        callback(null, './storage/subtitles/' + dir);
    },
    filename: function (request, file, callback) {
        if (request.body.filename) {
            callback(null, request.body.filename);
        } else {
            callback(null, file.originalname);
        }
    }
});

var uploadSubtitle = multer({
    storage: subtitleStorage,
    fileFilter: function (request, file, callback) {
        console.log(file);
        if (file.mimetype.split('/')[1] !== 'x-subrip') {
            console.log(file.mimetype.split('/')[1]);
            callback(new Error('Not a subtitle file!'), false);
        }
        callback(null, true);
    }
});

router.route('/subtitles')
    .post(uploadSubtitle.single('subtitle'), function (request, response) {
        seneca.act({role:'subtitle', cmd: 'add', subtitle: request.body}, function (error, subtitle) {
           if  (error) {
               response.send(error);
           } else {
               response.send(subtitle);
           }
        });
    })
    .get(function (request, response) {
        seneca.act({role: 'subtitle', cmd: 'list', query: request.query}, function (error, subtitles) {
            if (error) {
                response.send(error);
            } else {
                response.set({
                    'Access-Control-Allow-Origin': '*',
                    'Acess-Control-Allow-Methods': 'GET',
                    'Access-Control-Allow-Headers': 'Content-Type'
                });
                response.send(subtitles);
            }
        })
    });

router.route('/subtitles/:subtitle_id')
    .get(function (request, response) {
        seneca.act({role:'subtitle', cmd:'retrieve', id: request.params['subtitle_id']}, function (error, subtitle) {
            if (error) {
                response.send(error);
            } else {
                if (request.params['episode_id']) {

                } else {
                  response.send(subtitle);
                }
            }
        });
    })
    .put(function (request, response) {
        seneca.act({role: 'subtitle', cmd:'update', id: request.params['subtitle_id'], subtitle: request.body}, function (error, subtitle) {
           if (error) {
               response.send(error);
           } else {
               response.send(subtitle);
           }
        });
    })
    .delete(function (request, response) {
        seneca.act({role:'subtitle', cmd:'delete', id: request.params['subtitle_id']}, function (error, result) {
           if (error) {
               response.send(error);
           } else {
               response.send(result);
           }
        });
    });

router.get('/subtitles/:subtitle_id/download', function (request, response) {
    seneca.act({role:'subtitle', cmd:'retrieve', id: request.params['subtitle_id']}, function (error, subtitle) {
        console.log(subtitle);
        if (error) {
            response.send(error);
        } else {
            response.download(path.join(__dirname + '/../storage' + subtitle.storage.path));
        }
    });
});

router.get('/subtitles/:subtitle_id/stream', function (request, response) {
    seneca.act({role:'subtitle', cmd:'retrieve', id: request.params['subtitle_id']}, function (error, subtitle) {
        if (error) {
            response.send(error);
        } else {
            response.set({
                'Access-Control-Allow-Origin': '*',
                'Acess-Control-Allow-Methods': 'GET',
                'Access-Control-Allow-Headers': 'Content-Type'
            });
            var rstream = fs.createReadStream(path.join(__dirname + '/../storage' + subtitle.storage.path));
            rstream.pipe(response);
        }
    });
});

module.exports = router;
