var express = require('express');
var router = express.Router();

var season = require('./season');

var seneca = require('seneca')();
var series = require('../controllers/series');

seneca.add({role:'series', cmd:'add'}, series.addSeries);
seneca.add({role:'series', cmd:'list'}, series.listSeries);
seneca.add({role:'series', cmd:'retrieve'}, series.retrieveSeries);
seneca.add({role:'series', cmd:'update'}, series.updateSeries);
seneca.add({role:'series', cmd:'delete'}, series.deleteSeries);

router.route('/series')
    .post(function (request, response) {
        seneca.act({role:'series', cmd:'add', series: request.body}, function (error, series) {
            if (error) {
                response.send(error);
            } else {
                response.send(series);
            }
        })
    })
    .get(function (request, response) {
        seneca.act({role:'series', cmd:'list', query: request.query}, function (error, series) {
            if (error) {
                response.send(error);
            } else {
                response.set({
                    'Access-Control-Allow-Origin': '*',
                    'Acess-Control-Allow-Methods': 'GET',
                    'Access-Control-Allow-Headers': 'Content-Type'
                });
                response.send(series);
            }
        })
    });

router.route('/series/:id')
    .get(function (request, response) {
        seneca.act({role:'series', cmd:'retrieve', id: request.params.id}, function (error, series) {
            if (error) {
                response.send(error);
            } else {
                response.send(series);
            }
        })
    })
    .put(function (request, response) {
        seneca.act({role:'series', cmd:'update', id: request.params.id}, function (error, series) {
            if (error) {
                response.send(error);
            } else {
                response.send(series);
            }
        })
    })
    .delete(function (request, response) {
        seneca.act({role:'series', cmd:'delete', id: request.params.id, attached: request.query.attached}, function (error, result) {
            if (error) {
                response.send(error);
            } else {
                response.send(result);
            }
        })
    });

router.use('/series/:id/season', season);


module.exports = router;