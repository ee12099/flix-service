var express = require('express');
var router = express.Router();
var seneca = require('seneca')();
var search = require('../controllers/db');

seneca.add({role:'db', cmd:'search'}, search.searchMongoDB);

router.route('/db/search')
    .post(function (request, response) {
        console.log(request.body);
        seneca.act({role:'db', cmd:'search', collection: request.body.collection, query: request.body.query}, function (error, items) {
           if (error) {
               response.send(error);
           } else {
               response.send(items);
           }
        });
    });

module.exports = router;