var Movie = require('../models/movie').Movie;
var Video = require('../models/video').Video;

exports.addMovie = function (args, done) {
    Video.findById(args.video_id, function (error, video) {
        if (error) {
            done(error);
        } else {
            var movie = new Movie({
                title: args.movie.title,
                description: args.movie.description,
                year: args.movie.year,
                genre: args.movie.genre,
                director: args.movie.director,
                writer: args.movie.writer,
                cast: args.movie.cast,
                cover_url: args.movie.cover_url,
                video: video
            });
            movie.save(function (error) {
                if (error) {
                    done(error);
                } else {
                    done(null, movie);
                }
            });
        }
    });
};

exports.deleteMovie = function (args, done) {
  Movie.remove({
      _id: args.id
  }, function (error) {
        if(error) {
            done(error);
        } else {
            done(null, true);
        }
  });
};

exports.listMovies = function (args, done) {
    Movie.find(args.query).exec(function (error, movies) {
        if (error) {
            done(error);
        } else {
            done(null, movies);
        }
    });
};

exports.retrieveMovie = function (args, done) {
  Movie.findById(args.id, function (error, movie) {
      if (error) {
          done(error);
      } else {
          done(null, movie);
      }
  })
};

exports.updateMovie = function (args, done) {
    Movie.findByIdAndUpdate(args.id, args.movie, {new: true}, function (error, movie) {
        if (error) {
            done(error);
        } else {
            done(null, movie);
        }
    })
};