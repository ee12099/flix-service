var Series = require('../models/series').Series;
var Seasons = require('./season');

exports.addSeries = function (args, done) {
    var series = new Series({
        title: args.series.title,
        description: args.series.description,
        year: args.series.year,
        genre: args.series.genre,
        director: args.series.director,
        writer: args.series.writer,
        cast: args.series.cast,
        cover_url: args.series.cover_url,
        seasons: []
    });
    series.save(function (error) {
        if (error) {
            done(error);
        } else {
            done(null, series);
        }
    })
};

exports.addSeasonToSeries = function (args, done) {
  Series.findById(args.series, function (error, series) {
      if (error) {
          done(error);
      } else {
          var index = series.seasons.map(function (element) {
              return element.season;
          }).indexOf(args.season);
          series.seasons[index] = args.season;
          series.save(function (error) {
              if (error) {
                  done(error);
              } else {
                  done(null, series);
              }
          });
      }
  });
};

exports.deleteSeries = function (args, done) {
    /*
        Find the series based on the provided id property
     */
    Series.find({
        _id: args.id
    }, function (error, series) {
        if (error) {
            done(error);
        } else {
            /*
            If the flag for removing all attached elements was set to true
             */
            if (args.attached) {
                /*
                    Iterate the series seasons property Array
                 */
                for (var season in series) {
                    /*
                        Delete each season in the Array
                        by calling Seasons controller method deleteSeason(args, callback)
                     */
                    Seasons.deleteSeason({id: season._id, attached: args.attached}, function (error, result) {
                        if (error) {
                            done(error);
                        }
                    });
                }
                /*
                    Finally, when all the attached seasons were removed
                    remove the series object
                 */
                series.remove(function (error) {
                    if (error) {
                        console.log(error);
                    } else {
                        done(null, true);
                    }
                })
            } else {
                /*
                    If the attached objects were to persist
                    simply remove the series object
                 */
                series.remove(function (error) {
                    if (error) {
                        console.log(error);
                    } else {
                        done(null, true);
                    }
                })
            }
        }
    })
};

exports.listSeries = function (args, done) {
    Series.find(args.query).exec(function (error, series) {
        if (error) {
            done(error);
        } else {
            done(null, series);
        }
    });
};

exports.retrieveSeries = function (args, done) {
    Series.findById(args.id, function (error, series) {
        if (error) {
            done(error);
        } else {
            done(null, series);
        }
    })
};

exports.updateSeries = function (args, done) {
    Series.findByIdAndUpdate(args.id,  args.series, {new: true}, function (error, series) {
        if (error) {
            done(error);
        } else {
            done(null, series)
        }
    });
};
