var Series = require('../models/series').Series;
var Season = require('../models/season').Season;
var Episodes =  require('./episode');

exports.addSeason = function (args, done) {
    var season = Season({
        series: args.series,
        season: args.season.season,
        index: args.season.season,
        title: args.season.title,
        description: args.season.description,
        year: args.season.year,
        cover_url: args.season.cover_url,
        episodes: []
    });
    season.save(function (error) {
        if (error) {
            done(error);
        } else {
          /*
            if (args.series) {
                Series.findById(args.series, function (error, series) {
                    if (error) {
                        done(error);
                    } else {
                        series.seasons.push(season);
                        series.save(function (error) {
                            if (error) {
                                done(error);
                            } else {
                                done(null, season);
                            }
                        });
                    }
                });
            }
            */
            done(null, season);
        }
    });
};

exports.addEpisodeToSeason = function (args, done) {
  Season.findById(args.season, function (error, season) {
      if (error) {
          done(error);
      } else {
          season.episodes.push(args.episode);
          season.episodes.sort(function (a, b) {
              return a.index - b.index;
          });
          season.save(function (error) {
              if (error) {
                  done(error);
              } else {
                    done(null, season);
              }
          });
      }
  });
}

exports.deleteSeason = function (args, done) {
    /*
        Find the season based on the provided id property
     */
    Season.find({
        _id: args.id
    }, function (error, season) {
        if(error) {
            done(error);
        } else {
            /*
                Find the series witch the season is attached to
             */
            Series.find({
                _id: args.series
            }, function (error, series) {
                if (error) {
                    done(error);
                } else {
                    /*
                        Remove the season from the series seasons property Array
                     */
                    const index = series.seasons.indexOf(season);
                    series.seasons.splice(index,  1);
                    /*
                        If the flag for removing all attached elements was set to true
                     */
                    if (args.attached = true) {
                        /*
                            Iterate the season episodes property Array
                         */
                        for (var episode in season.episodes) {
                            /*
                                Delete each episode in the Array
                                by calling Episodes controller method deleteEpisode(args, callback)
                             */
                            Episodes.deleteEpisode({id: episode._id, attached: args.attached}, function (error, result) {
                                if (error) {
                                    done(error);
                                }

                            });
                        }
                        /*
                            Finally, when all the attached episodes were removed
                            remove the season object
                         */
                        season.remove(function (error) {
                            if (error) {
                                done(error);
                            } else {
                                done(null, true);
                            }
                        });
                    } else {
                        /*
                            If the flag for removing was not set (false by default)
                            remove the season object
                         */
                        season.remove(function (error) {
                            if (error) {
                                done(error);
                            } else {
                                done(null, true);
                            }
                        });
                    }
                }
            })
        }
    });
};
/*
exports.listSeasons = function (args, done) {
    Season.find(args.query).exec(function (error, seasons) {
        if (error) {
            done(error);
        } else {
            done(null, seasons);
        }
    });
};
*/
exports.listSeasons = function (args, done) {
    Season.find({series: args.series}).exec(function (error, seasons) {
        if (error) {
            done(error);
        } else {
            done(null, seasons);
        }
    });
};
exports.retrieveSeason = function (args, done) {
    Season.find({
        series: args.series,
        _id: args.id
    }, function (error, season) {
        if (error) {
            done(error);
        } else {
            done(null, season);
        }
    })
};

exports.updateSeason = function (args, done) {
    Season.find({
        series: args.series,
        _id: args.id
    }, function (error, season) {
        if (error) {
            done(error);
        } else {
            season.update(args.season, {new: true}, function (error, season) {
                if (error) {
                    done(error);
                } else {
                    done(null, season);
                }
            })
        }
    })
};
