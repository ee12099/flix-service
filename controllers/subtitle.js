var Subtitle = require('../models/subtitle').Subtitle;

exports.addSubtitle = function (args, done) {
    var subtitle = new Subtitle({
        title: args.subtitle.title,
        language: args.subtitle.language,
        storage: {
           filename: args.subtitle.filename
        }
    });
    subtitle.save(function (error) {
        if (error) {
            done(error);
        } else {
            done(null, subtitle);
        }
    });
};

exports.deleteSubtitle = function (args, done) {
  Subtitle.remove({
      _id: args.id
  }, function (error) {
      if (error) {
          done(error);
      } else {
          done(null, true)
      }
  });
};

exports.listSubtitles = function (args, done) {
  Subtitle.find(args.query).exec(function (error, subtitles) {
      if (error) {
          done(error);
      } else {
          done(null, subtitles);
      }
  });
};

exports.retrieveSubtitle = function (args, done) {
  Subtitle.findById(args.id, function (error, subtitle) {
      if (error) {
          done(error);
      } else {
          done(null, subtitle)
      }
  });
};

exports.updateSubtitle = function (args, done) {
  Subtitle.findByIdAndUpdate(args.id, args.subtitle, {new: true}, function (error, subtitle) {
      if (error) {
          done(error);
      } else {
          done(null, subtitle);
      }
  });
};
