var Video = require('../models/video').Video;

exports.addVideo = function (args, done) {
    var video = new Video({
        storage: {
           filename: args.video.filename
        }
    });
    video.save(function (error) {
        if (error) {
            done(error);
        } else {
            done(null, video);
        }
    });
};

exports.deleteVideo = function (args, done) {
  Video.remove({
      _id: args.id
  }, function (error) {
      if (error) {
          done(error);
      } else {
          done(null, true)
      }
  });
};

exports.listVideos = function (args, done) {
  Video.find(args.query).exec(function (error, videos) {
      if (error) {
          done(error);
      } else {
          done(null, videos);
      }
  });
};

exports.retrieveVideo = function (args, done) {
  Video.findById(args.id, function (error, video) {
      if (error) {
          done(error);
      } else {
          done(null, video)
      }
  });
};

exports.updateVideo = function (args, done) {
  Video.findByIdAndUpdate(args.id, args.video, {new: true}, function (error, video) {
      if (error) {
          done(error);
      } else {
          done(null, video);
      }
  });
};

