var Series = require('../models/series').Series;
var Season = require('../models/season').Season;
var Episode = require('../models/episode').Episode;
var Video = require('../models/video').Video;

exports.addEpisode = function (args, done) {
    Video.findById(args.video_id, function (error, video) {
        if (error) {
            done(error);
        } else {
            var episode = new Episode({
                season: args.season,
                index: args.episode.index,
                title: args.episode.title,
                description: args.episode.description,
                cover_url: args.episode.cover_url,
                video: video
            });
            episode.save(function (error) {
                if (error) {
                    done(error);
                } else {
                  /*
                    if (args.season) {
                        Season.findById(args.season, function (error, season) {
                            if (error) {
                                done(error);
                            } else {
                                season.episodes.push(episode);
                                season.episodes.sort(function (a, b) {
                                    return a.index - b.index;
                                });
                                season.save(function (error) {
                                    if (error) {
                                        done(error);
                                    } else {
                                        Series.findById(season.series, function (error, series) {
                                            if (error) {
                                                done(error);
                                            } else {
                                                var index = series.seasons.map(function (element) {
                                                    return element.season;
                                                }).indexOf(args.season);
                                                series.seasons[index] = season;
                                                series.save(function (error) {
                                                    if (error) {
                                                        done(error);
                                                    } else {
                                                        done(null, episode);
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                    */
                    done(null, episode);
                }
            });

        }
    });
};

exports.addSubtitleToEpisode = function (args, done) {
  Episode.findById(args.episode, function (error, episode) {
      if (error) {
          done(error);
      } else {
          var index = episode.subtitles.map(function (element) {
              return element.subtitle;
          }).indexOf(args.subtitle);
          episode.subtitles[index] = args.subtitle;
          episode.save(function (error) {
              if (error) {
                  done(error);
              } else {
                  done(null, episode);
              }
          });
      }
  });
};

exports.deleteEpisode = function (args, done) {
    Episode.find({
        _id: args.id
    }, function (error, episode) {
        if (error) {
            done(error);
        } else {
            Season.find({
                _id: args.season
            }, function (error, season) {
                if (error) {
                    done(error);
                } else {
                    const index = season.episodes.indexOf(episode);
                    season.episodes.splice(index, 1);
                    if (args.attached = true) {
                        Video.remove({
                            _id: episode.video._id
                        }, function (error) {
                            if (error) {
                                done(error);
                            } else {
                                episode.remove(function (error) {
                                    if (error) {
                                        done(error);
                                    } else {
                                        done(null, true);
                                    }
                                });
                            }
                        })
                    }
                }
            })
        }
    });
};

/*
exports.listEpisodes = function (args, done) {
    Episode.find(args.query).exec(function (error, episodes) {
        if (error) {
            done(error);
        } else {
            done(null, episodes);
        }
    });
};
*/
exports.listEpisodes = function (args, done) {
    Episode.find({season: args.season}).exec(function (error, episodes) {
        if (error) {
            done(error);
        } else {
            done(null, episodes);
        }
    });
};
/*
exports.retrieveEpisode = function (args, done) {
    Episode.findById(args.id, function (error, episode) {
        if (error) {
            done(error);
        } else {
            done(null, episode);
        }
    })
};
*/
exports.retrieveEpisode = function (args, done) {
    Episode.findOne({season: args.season, _id: args.id}, function (error, episode) {
        if (error) {
            done(error);
        } else {
            done(null, episode);
        }
    })
};
/*
exports.updateEpisode = function (args, done) {
    Episode.findByIdAndUpdate(args.id, args.episode, {new: true}, function (error, episode) {
        if (error) {
            done(error);
        } else {
            done(null, episode);
        }
    })
};
*/
exports.updateEpisode = function (args, done) {
    Episode.find({season: args.season}).exec(function (error, episodes) {
        if (error) {
            done(error);
        } else {
            episodes.findByIdAndUpdate(args.id, args.episode, {new: true}, function (error, episode) {
                if (error) {
                    done(error);
                } else {
                    done(null, episode);
                }
            })
        }
    })
};
