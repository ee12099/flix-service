var db = require('../models/db').Mongodb;

exports.searchMongoDB = function (args, done) {
    db.get().collection(args.collection).find(args.query, function (error, cursor) {
        if (error) {
            done(error);
        } else {
            cursor.toArray().then(function (items) {
                done(null, items)
            }).catch(function (error) {
                done(error);
            });
        }
    });

};
