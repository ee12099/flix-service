// import libraries
var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var fs = require('fs');
var path = require('path');
var morgan = require('morgan');
var seneca = require('seneca')();

// import local modules

// import db_config
var db_config = require('./config/db');
// import routes
var videos = require('./routes/video');
var subtitles = require('./routes/subtitle');
var movies = require('./routes/movie');
var series = require('./routes/series');
var db = require('./routes/db');

seneca.add({role:'mongoose', cmd:'connect'}, db_config.mongoose);
seneca.add({role:'mongodb', cmd:'connect'}, db_config.connect);

seneca.act({role:'mongoose', cmd:'connect', database:'mean-services'}, function (error, done) {
  if (error) {
    console.log(error);
  } else {
    console.log(done);
  }
});

seneca.act({role:'mongodb', cmd:'connect', database:'mean-services'}, function (error, db) {
  if (error) {
    console.log(error);
  }
});

var app = express();

app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(express.static('front-end'));
app.use(express.static('storage'));

app.use('/api', videos);
app.use('/api', subtitles);
app.use('/api', movies);
app.use('/api', series);
app.use('/api', db);

app.get('/', function (request, response) {
   response.sendFile(path.join(__dirname + '/frontend/index.html'));
});

/*
app.get('/videos/stream', function (request, response) {
    var filename = request.query.filename;
    var file = path.join(__dirname + '/videos/' + filename);
    fs.exists(file, function(exists) {
        if (exists) {
            var rstream = fs.createReadStream(file);
            rstream.pipe(response);
        } else {
            response.status(400);
            response.end();
        }
    });
});

app.get('/videos/download', function (request, response) {
    var filename = request.query.filename;
    var file = path.join(__dirname + '/videos/' + filename);
    fs.exists(file, function(exists) {
        if (exists) {
            response.setHeader('Content-disposition', 'attachment; filename=' + filename);
            response.setHeader('Content-Type', 'video/*');
            var rstream = fs.createReadStream(file);
            rstream.pipe(response);
        } else {
            response.status(400);
            response.end();
        }
    });
});
*/

app.listen(5000, function () {
    console.log('App listening on port 5000');
});

exports.app = app;
